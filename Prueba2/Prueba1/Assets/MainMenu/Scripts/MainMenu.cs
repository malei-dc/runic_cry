﻿//<summary>
// Main menu
// Attached to Main Camera
// </sumary>
using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {
	public Texture BackgroundTexture;


	void OnGUI(){

		//Display background texture
		GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), BackgroundTexture);
		//Display our Buttons
		if (GUI.Button (new Rect (Screen.width * .42f, Screen.height * .4f, Screen.width * .15f, Screen.height * .05f), "Nueva Partida")){
			Application.LoadLevel(4);

		}
		if (GUI.Button (new Rect (Screen.width * .42f, Screen.height * .5f, Screen.width * .15f, Screen.height * .05f), "Creditos")){
			Application.LoadLevel(2);
			
		}
		if (GUI.Button (new Rect (Screen.width * .42f, Screen.height * .6f, Screen.width * .15f, Screen.height * .05f), "Salir")){
			
			Application.Quit();
		}
	}

}
